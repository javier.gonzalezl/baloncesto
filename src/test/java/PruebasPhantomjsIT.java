import java.util.concurrent.TimeUnit;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PruebasPhantomjsIT {
    private static WebDriver driver = null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void resetVotosTest(){
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);

        //Entramos a la página principal
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        //Pulsamos el botón de poner votos a 0
        driver.findElement(By.name("B3")).click();

        //Pulsamos el botón para acceder a la vista de los votos
        driver.findElement(By.name("B4")).click();

        /* Localizamos el elemento tbody de la tabla. Hecho esto, iteramos sobre cada fila (tr) y
        comprobamos si la 2ª columna es 0.  */
        boolean result = true;
        List<WebElement> tableRows = driver.findElements(By.xpath("//table/tbody/tr"));
        for(WebElement row : tableRows) {
            int votos = Integer.parseInt(row.findElements(By.tagName("td")).get(1).getText());
            if (votos != 0){
                result = false;
                break;
            }
            System.out.println(row.findElements(By.tagName("td")).get(0).getText() + ":" + votos);
        }
        System.out.println("Result:"+ result);

        assertTrue(result);

        driver.close();
        driver.quit();
    }

    @Test
    public void votarOpcionOtroTest(){
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });

        driver = new PhantomJSDriver(caps);

        String nombreJugador = "Tavares";
        //Entramos a la página principal
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        // Seleccionamos el radio button con valor Otros e introducimos el nombre de un jugador
        driver.findElement(By.xpath("//input[@type='radio' and @value='Otros']")).click();
        driver.findElement(By.name("txtOtros")).sendKeys(nombreJugador);
        
        // Enviamos el formulario
        driver.findElement(By.name("txtOtros")).submit();

        // Regresamos a la página principal
        driver.findElement(By.partialLinkText("Ir")).click();
        
        //Pulsamos el botón para acceder a la vista de los votos
        driver.findElement(By.name("B4")).click();

        /* Localizamos el elemento tbody de la tabla. Hecho esto, iteramos sobre cada fila (tr) y
        comprobamos si existe el jugador al que acabamos de votar y si su nº de votos es 1-  */
       boolean result = false;
        List<WebElement> tableRows = driver.findElements(By.xpath("//table/tbody/tr"));
        for(WebElement row : tableRows) {
            String nombre = row.findElements(By.tagName("td")).get(0).getText();
            int votos = Integer.parseInt(row.findElements(By.tagName("td")).get(1).getText());
            System.out.println("Revisando votos de "+nombre+"...");
            if(nombre.equals(nombreJugador) && votos == 1){
                result = true;
                break;
            }
        }
        System.out.println("Result:"+ result);

        assertTrue(result);

        driver.close();
        driver.quit();
    }
}
