import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ModeloDatosTest {

    /*@Test
    public void testExisteJugador(){
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos();
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        //fail("Fallo forzado.");
    }*/

    @Test
    public void testActualizarJugadorShouldPassOnlyIfExceptionIsCaught() {
        System.out.println("Prueba de actualizarJugador");
        ModeloDatos instance = new ModeloDatos();
        assertThrows(Exception.class, () -> instance.actualizarJugador("Llull"),
                "La operación debe lanzar una excepción si no se ha establecido una conexión con la BBDD");
    }
}
