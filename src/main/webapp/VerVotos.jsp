<%@ page import="java.util.HashMap" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Listado de Votos</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" href="estilos.css" type="text/css">
</head>
<body>
<p class="big-font">Votaci&oacute;n al mejor jugador de la liga ACB</p>

<table>
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Votos</th>
    </tr>
    </thead>
    <tbody>
    <%
        HashMap<String, Integer> jugadores = (HashMap<String, Integer>)session.getAttribute("jugadores");
        for(String nombre: jugadores.keySet()){
            out.println("<tr>");
            out.println("<td>"+nombre+"</td>");
            out.println("<td>"+jugadores.get(nombre)+"</td>");
            out.println("</tr>");
        }
    %>
    </tbody>
</table>
<br>


<br> <a href="index.html"> Ir al comienzo</a>
</body>
</html>
