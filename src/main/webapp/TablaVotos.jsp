<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Votaci&oacute;n mejor jugador liga ACB</title>
        <link rel="stylesheet" href="estilos.css" type="text/css">
    </head>
    <body class="resultado">
        <p class="big-font">Votaci&oacute;n al mejor jugador de la liga ACB</p>
        <hr>
        <%
            String nombreP = (String) session.getAttribute("nombreCliente");
        %>
        <br>
        <p class="big-font">Muchas gracias <%=nombreP%> por su voto</p>

        <br>
        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>
