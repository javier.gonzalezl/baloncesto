
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.*;
import javax.servlet.http.*;

public class Votos extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        try{
            Map<String, Integer> jugadores = new HashMap<String, Integer>();
            jugadores = bd.obtenerJugadores();
            s.setAttribute("jugadores", jugadores);
            System.out.println(jugadores);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
    }


    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}