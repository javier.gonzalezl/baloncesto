import java.sql.*;
import java.util.*;

public class ModeloDatos {

    private Connection con;
    private Statement set;
    private ResultSet rs;
    private final String ERROR_MSG = "El error es: ";

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            System.out.println("No se ha podido conectar");
            System.out.println(ERROR_MSG + e.getMessage());
        }
    }

    public boolean existeJugador(String nombre) throws Exception {
        boolean existe = false;
        String cad;

        set = con.createStatement();
        rs = set.executeQuery("SELECT * FROM Jugadores");
        while (rs.next()) {
            cad = rs.getString("Nombre");
            cad = cad.trim();
            if (cad.compareTo(nombre.trim()) == 0) {
                existe = true;
            }
        }
        rs.close();
        set.close();
        return (existe);
    }

    public void actualizarJugador(String nombre) throws Exception {
        set = con.createStatement();
        set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
        rs.close();
        set.close();
    }

    public void insertarJugador(String nombre) throws Exception {
        set = con.createStatement();
        set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
        rs.close();
        set.close();
    }

    public void resetearVotos() throws Exception {
        set = con.createStatement();
        set.executeUpdate("UPDATE Jugadores SET votos=0");
        rs.close();
        set.close();
    }

    public Map<String, Integer> obtenerJugadores() throws SQLException {
        HashMap<String, Integer> jugadores = new HashMap<String, Integer>();

        set = con.createStatement();
        rs = set.executeQuery("SELECT * FROM Jugadores");

        while (rs.next()) {
            String nombre = rs.getString("nombre");
            int votos = rs.getInt("votos");
            jugadores.put(nombre, votos);
        }

        rs.close();
        set.close();
        return jugadores;
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
